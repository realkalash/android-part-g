package com.example.androidfiles;


import java.util.List;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public class ApiService {
    private static final String API = "https://restcountries.eu/rest/v2/";
    private static PrivateApi privateApi;

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);

    }

    public static Observable<RequestModelWithCode> getCountryByCode(String countryText) {
            return privateApi.getCountryByCode(countryText);
    }
    public static Observable<List<RequestModel>> getCountryByName(String countryText) {
        return privateApi.getCountryByName(countryText);
    }

    public interface PrivateApi {
        @GET("alpha/{code}")
        Observable<RequestModelWithCode> getCountryByCode(@Path("code") String code);

        @GET("name/{name}")
        Observable<List<RequestModel>> getCountryByName(@Path("name") String name);
    }

}
