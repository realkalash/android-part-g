package com.example.androidfiles;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.am_btnGetCurrencyInfo)
    Button btnConnect;
    @BindView(R.id.am_editTextCountry)
    EditText editTextNameCountry;
    @BindView(R.id.am_NameCountry)
    TextView txtNameCountry;
    @BindView(R.id.am_CurrencyCountry)
    TextView txtCurrencyCountry;
    @BindView(R.id.am_languageCountry)
    TextView txtLanguageCountry;
    @BindView(R.id.am_capitalCountry)
    TextView txtCapitalCountry;

    Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


    }

    @OnClick(R.id.am_btnGetCurrencyInfo)
    public void getRequest() {
        String nameCountry = editTextNameCountry.getText().toString();
        if (nameCountry.length() <= 3)
            getRequestByCode(nameCountry);
        else getRequestByName(nameCountry);

    }

    private void getRequestByName(String nameCountry) {
        Disposable subscribe = ApiService.getCountryByName(nameCountry)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<RequestModel>>() {
                    @Override
                    public void accept(List<RequestModel> requestModels) throws Exception {
                        RequestModel itemCountry = requestModels.get(0);

                        txtNameCountry.setText(String.format("Name Country: %s", itemCountry.getName()));
                        txtLanguageCountry.setText(String.format("Language Country: %s", itemCountry.getLanguages().get(0).getName()));
                        txtCapitalCountry.setText(String.format("Capital Country: %s", itemCountry.getCapital()));
                        txtCurrencyCountry.setText(String.format("%s %s %s",
                                itemCountry.getCurrencies().get(0).getName(),
                                itemCountry.getCurrencies().get(0).getCode(),
                                itemCountry.getCurrencies().get(0).getSymbol()));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(MainActivity.this, "I can't. Sorry", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void getRequestByCode(String nameCountry) {
        Disposable subscribe = ApiService.getCountryByCode(nameCountry)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<RequestModelWithCode>() {
                    @Override
                    public void accept(RequestModelWithCode requestModels) throws Exception {

                        RequestModelWithCode itemCountry = requestModels;

                        txtNameCountry.setText(String.format("Name Country: %s", itemCountry.getName()));
                        txtLanguageCountry.setText(String.format("Language Country: %s", itemCountry.getLanguages().get(0).getName()));
                        txtCapitalCountry.setText(String.format("Capital Country: %s", itemCountry.getCapital()));
                        txtCurrencyCountry.setText(String.format("%s %s %s",
                                itemCountry.getCurrencies().get(0).getName(),
                                itemCountry.getCurrencies().get(0).getCode(),
                                itemCountry.getCurrencies().get(0).getSymbol()));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(MainActivity.this, "Error !", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
